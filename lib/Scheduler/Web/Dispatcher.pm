package Scheduler::Web::Dispatcher;
use strict;
use warnings;
use utf8;
use Amon2::Web::Dispatcher::RouterBoom;

use Time::Piece;

get '/' => sub {
    my ($c) = @_;

    my $order = $c->req->parameters->{order} // '';

    my @schedules;
    if ( $order eq 'reverse' ) {
        @schedules = $c->db->search('schedules', {}, { order_by => 'date ASC'});
    } else {
        @schedules = $c->db->search('schedules', {}, { order_by => 'date DESC'});
    };
    return $c->render('index.tx', { schedules => \@schedules });
};

post '/post' => sub {
    my ($c) = @_;

    $c->db->insert('schedules' => {
        title => $c->req->parameters->{title},
        date  => $c->req->parameters->{date},
    });

    return $c->redirect('/');
};

get '/schedules/:id/edit' => sub {
    my ($c, $args) = @_;
    my $id = $args->{id};

    my $schedule = $c->db->single('schedules', { id => $id });
    return $c->render('edit.tx', { schedule => $schedule });
};

post '/schedules/:id/update' => sub {
    my ($c, $args) = @_;
    my $id = $args->{id};

    $c->db->update('schedules',
        {
            title => $c->req->parameters->{title},
            date  => $c->req->parameters->{date},
        },
        {
            id => $id,
        },
    );

    return $c->redirect('/');
};

post '/schedules/:id/delete' => sub {
    my ($c, $args) = @_;
    my $id = $args->{id};

    $c->db->delete('schedules' => { id => $id});
    return $c->redirect('/');
};

1;
